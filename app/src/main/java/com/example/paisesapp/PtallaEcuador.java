package com.example.paisesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.MediaController;
import android.widget.VideoView;

public class PtallaEcuador extends AppCompatActivity {

    VideoView video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ptalla_ecuador);

        DisplayMetrics medidaVentana = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidaVentana);

        int ancho = medidaVentana.widthPixels;
        int alto = medidaVentana.heightPixels;

        getWindow().setLayout((int)(ancho * 0.85), (int)(alto * 0.7));

        video = (VideoView) findViewById(R.id.btnvideo);
        Uri uri =  Uri.parse("http://techslides.com/demos/sample-videos/small.mp4");
        //Uri uri =  Uri.parse("https://youtu.be/YJtOuk-Cu0Y");

        video.setMediaController((new MediaController(this)));
        video.setVideoURI(uri);
        video.requestFocus();
        video.start();

    }
}
