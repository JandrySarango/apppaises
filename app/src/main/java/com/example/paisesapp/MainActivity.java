package com.example.paisesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button ecuador;
    private Button brazil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ecuador = findViewById(R.id.btnEcuador);
        brazil = findViewById(R.id.btnBrazil);


        ecuador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(MainActivity.this, PtallaEcuador.class));
                Intent btDatos = new Intent(MainActivity.this, PtallaEcuador.class);
                startActivity(btDatos);

            }
        });

        brazil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btDatos = new Intent(MainActivity.this, PtallaBrazil.class);
                startActivity(btDatos);
            }
        });

    }
}
